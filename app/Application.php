<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //
    protected $fillable = ['name'];

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

