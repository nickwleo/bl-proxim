<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    public function list(Request $request) {
        if ($request->user()->role == 'admin') {
            $applications = Application::all();
        }
        else {
            $applications = Application::where('user_id', $request->user()->id)->get();
        }
        return view('application.list', ['applications' => $applications]);
    }

    public function createForm(){
        $application = new Application();
        //Set default values here if any
        return view('application.create', ['application' => $application]);
    }

    public function create(Request $request) {
        $application = new Application();
        $application->loan_amount = $request->input('loan_amount');
        $application->monthly_income = $request->input('monthly_income');
        $application->employment = $request->input('employment');
        $application->phone = $request->input('phone');
        $application->address1 = $request->input('address1');
        $application->address2 = $request->input('address2');
        $application->user_id = $request->user()->id;
        $application->save();
        return redirect('applications/' . $application->id . "/documents");
    }
}
