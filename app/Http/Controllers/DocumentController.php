<?php

namespace App\Http\Controllers;

use App\Application;
use App\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function viewDocumentsForApplication(Request $request){
        $application = Application::with(['documents'])->findOrFail($request->route('id'));
        return view('document.view', ['application' => $application]);
    }

    public function uploadDocument(Request $request) {
        $path = $request->file('newdocument')->store('documents', 'public');
        $document = new Document();
        $document->title = $request->input('title');
        $document->fileUrl = "/storage/" . $path;
        $document->application_id = $request->route('id');
        $document->save();
        return redirect('applications/' . $request->route('id') . "/documents");
    }

    public function deleteDocument(Request $request) {
        Document::where('id',$request->route('document_id'))->delete();
        return redirect('applications/' . $request->route('id') . "/documents");
    }
}
