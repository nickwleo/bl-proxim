@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">New Loan Application</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            {!! Form::open(['url' => 'applications', 'method' => 'post',  'class'=>'form']) !!}

                            @include('partials.application')

                            {!! Form::submit('Create Application', ['class' => 'form-control btn-brawta-purple']) !!}

                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
