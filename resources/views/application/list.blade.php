@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Applications</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <button onclick="window.location='{{ url('applications/create') }}'" type="button" class="btn btn-brawta-purple">New Loan Application</button>
                        <br><br>
                        <div class="table-responsive">
                            <table style="max-width:100% !important;" id="applicationsTable" class="table table-striped">
                                <thead>
                                <tr><td>Loan Documents</td><td>Applicant Name</td><td>Email</td><td class="desktop">Amount Requested</td><td>Monthly Income</td><td>Employment</td><td>Address</td><td>Status</td><td>Application Date</td></tr>
                                </thead>
                                <tbody>
                                @foreach($applications as $application)
                                    <tr>
                                        <td class="desktop"> <b><a href="{{ url('/applications/' . $application->id . '/documents') }}">View/Upload</a></b></td>
                                        <td>{{ $application->user->name }}</td>
                                        <td>{{ $application->user->email }}</td>
                                        <td class="desktop"> ${{ $application->loan_amount }} </td>
                                        <td class="desktop"> ${{ $application->monthly_income }} </td>
                                        <td class="desktop"> {{ $application->employment }}</td>
                                        <td class="desktop"> {{ $application->address1 }},&nbsp;{{ $application->address2 }}</td>
                                        <td class="desktop"> {{ $application->status }}</td>
                                        <td class="desktop"> {{ $application->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
