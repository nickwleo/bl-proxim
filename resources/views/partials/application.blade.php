
{!! Form::token() !!}

<div class="form-group">
    {!! Form::label('name', 'How Much Do You Need?') !!}
    {!! Form::text('loan_amount', $application->loan_amount,  ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('address1', 'How Much Do You Earn Monthly?') !!}
    {!! Form::text('monthly_income', $application->monthly_income,  ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('countryIso', 'What Is The Nature Of Your Employment?') !!}
    <select name="employment" class="form-control" required>
        <option value="Full-Time">Full-Time</option>
        <option value="Part-Time">Part-Time</option>
        <option value="Self-Employed">Self-Employed</option>
        <option value="Unemployed">Unemployed</option>
    </select>
</div>

<div class="form-group">
    {!! Form::label('address2', 'Address Line 1') !!}
    {!! Form::text('address1', $application->address1,  ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('address2', 'Address Line 2') !!}
    {!! Form::text('address2', $application->address2,  ['class' => 'form-control']) !!}
</div>

<br>
