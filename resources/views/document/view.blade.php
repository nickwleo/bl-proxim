@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">Loan Documents for {{ $application->user->name }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <center>
                            <h3>Supporting Documents</h3>
                            <br>
                            @if (sizeof($application->documents) > 0)
                                <ul>
                                    @foreach($application->documents as $document)
                                        <li><a href="{{ $document->fileUrl }}">{{ $document->title }}</a> [<a href="{{ 'documents/' . $document->id . '/delete' }}">Delete</a>]</li>
                                    @endforeach
                                </ul>
                            @else
                                <p>No documents have been uploaded for this application yet.</p>
                            @endif
                        </center>

                        {!! Form::open(['url' => 'applications/' . $application->id . '/documents', 'method' => 'post',  'class'=>'form', 'enctype'=>'multipart/form-data']) !!}

                            <div class="form-group">
                                {!! Form::label('title', 'Document Type') !!}
                                {!! Form::text('title', null,  ['class' => 'form-control', 'required' => 'required']) !!}
                            </div>

                            <center>
                                <br>
                                Upload New Document From Your PC <input required name="newdocument" type="file">
                                <br><br>
                            </center>

                            {!! Form::submit('Upload Document', ['class' => 'form-control btn-brawta-purple']) !!}

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
