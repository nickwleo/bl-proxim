<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('loan_amount');
            $table->decimal('monthly_income');
            $table->enum('employment', ['Full-Time','Part-Time','Self-Employed','Unemployed']);
            $table->string('phone')->nullable();
            $table->enum('status', ['Pending','Approved','Denied'])->default('Pending');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
