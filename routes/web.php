<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('applications');
    //return view('applications');
})->middleware('auth');

Auth::routes();

Route::get('/home', function () {
    return redirect('applications');
    //return view('applications');
})->middleware('auth');

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/applications', 'ApplicationController@list')->middleware('auth')->name('applications');
Route::get('/applications/create', 'ApplicationController@createForm')->middleware('auth')->name('application-create');
Route::post('/applications', 'ApplicationController@create')->middleware('auth');
Route::get('/applications/{id}/documents', 'DocumentController@viewDocumentsForApplication')->middleware('auth');
Route::post('/applications/{id}/documents', 'DocumentController@uploadDocument')->middleware('auth');
Route::get('/applications/{id}/documents/{document_id}/delete', 'DocumentController@deleteDocument')->middleware('auth');
